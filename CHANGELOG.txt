
-- 2008-02-12 version drupal-6-x-1-1-beta
* fixed issues with handling multiple user profiles, it is now possible
 to define role precedence
* introduced the global profile, it is possible to enable/disable FCKeditor globally
 for all profiles in one place
* it is possible to use paths and fields to exclude/include FCKeditor to have better control
 over where FCKeditor should appear
* UserFilesPath and UserFilesAbsolutePath are now configurable in administration panel
* path to fckstyles.xml is configurable in administration panel
* textarea remains resizeable when FCKeditor works in popup mode
* improved profile validation
* translated strings are now properly escaped
* fixed issue with break tag appearing at the beginning of a node
* separate config file included - makes configuration of built-in file browser much easier
* fixed issue with justifying
* added possibility to define custom javascript configuration for selected profiles
* added possibility to create profile even if there is no role with 'access fckeditor' permission
* FCKeditor module now passes the Code review test

-- 2007-11-15 version drupal-6-x-1-0-dev
* ported 5.x-2.x module
* improved teaser plugin integration (break button is enabled by default)